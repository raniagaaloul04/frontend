


export const requests = {
    authenticationApi: {
        signin: "/programmers/signin",
        signup: "/programmers/signup"
      
    },
    AddWhishlistApi: {
       add: "/whishlists/add",
       all: "/whishlists/all"
       
        
    },
    ProductsApi:{
        all: "/products/all",
        add:"/products/add"
    }
}