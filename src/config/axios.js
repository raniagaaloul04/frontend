import axios from "axios"
export const Axios=axios.create({
    baseURL:'http://localhost:5000'}
)
axios.interceptors.response.use((response) => {
    return response;
}, (error) => {
    return Promise.resolve({ error });
});