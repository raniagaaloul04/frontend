import React from 'react';
// import { Navbar } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import signin from './layouts/signin';
import signup from './layouts/signup';


import SidebarWhishlists from './layouts/SidebarWhishlists';
import Navbar from './layouts/Navbar';
import whishlist from './layouts/whishlist';
import SidebarProducts from './layouts/SidebarProducts';
import AjouterProduit from './layouts/AjouterProduit';





// import Navbar from './layouts/Navbar';
// import SidebarWhishlists from './layouts/SidebarWhishlists';


function App() {
  return (
    <div>
      <Router>
        <Switch>


          <Route path='/whishes' exact component={SidebarWhishlists} />
          <Route path='/whishes/:Name' component={whishlist} />
          <Route path='/products' exact component={SidebarProducts} />
          <Route path='/products/ajouterproduit' exact component={AjouterProduit} />
          <Route path='/products/:Title' exact component={AjouterProduit} />
      
          <Route path='/signup' component={signup} />
          <Route path='/' component={signin} />
          {/* <Route path='/SidebarWhishlists' component={SidebarWhishlists} /> */}
        </Switch>

      </Router>
   {/*    <SidebarWhishlists /> */}
      {/* <Acceuil /> */}
      {/* <SidebarWhishlists/> */}
      {/* <Router>
     


      
     */}

    </div>
  );
}

export default App;
