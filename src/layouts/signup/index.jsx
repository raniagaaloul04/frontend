import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import './index.css'
import { useState } from 'react';
import { BiLogIn } from 'react-icons/bi';
// import { Link } from 'react-router-dom'
import { Axios } from '../../config/axios';
import { requests } from '../../config/apirouter';

export default () => {
const [Name,setName] = useState("")
const [Password,setPassword] = useState("")
const [IsError,setIsError] = useState("")

const [signdata, setsigndata] = useState({
    Name: '',
    Password: '',


})
    



const [signerors, setsignrerors] = useState([])
const [issignerorr, setissignerorr] = useState(false)
const [isloadingsign, setisloadingsign] = useState(false)
const SignInputError = ({ name }) => {
    return (
        <>
            {
               issignerorr && isloadingsign
                    ?
                    signerors.map((detail, i) => {
                        return (
                            <>
                                {
                                    detail.path[0] === name
                                    &&
                                    <span style={{ color: 'red' }} >{detail.message}</span>
                                }
                            </>
                        )
                    })
                    : null

            }
        </>
    )
}
const handlechange = e => {
    const { name, value } = e.target;
    setsigndata(prevState => ({
        ...prevState,
        [name]: value
    }));
};
const sign = () => {


    Axios.post(requests.authenticationApi.signup ,signdata)
    .then(res=>{
        console.log(res);
        if(!res.data.success){
            setissignerorr(true)
            setsignrerors(res.data.errors.detail)
        }
    })}



    return (
        <>


            <div className="sign" style={{maxWidth:400,marginLeft:500,marginTop:150}}>
                <h3 class="pb-3" style={{marginLeft:150,marginTop:20}}>Sign Up</h3>



                <div className="mb-3">
                    <label className="form-label"style={{color:'gray',marginLeft:20}}>Name</label>
                    <br />

                    <input style={{marginLeft:20,maxWidth:300}} type="text" id="exampleInputName"  class="form-control"  name="Name" value={signdata.Name} onChange={handlechange}/>
                    <SignInputError name='Name' />


                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputPassword1" className="form-label" style={{color:'gray',marginLeft:20}}>Password</label>
                    <br />

                    <input type="password"style={{maxWidth:300,marginLeft:20}} id="exampleInputPassword1"  class="form-control"  name="Password" value={signdata.Password} onChange={handlechange}/>
                    <SignInputError name='Password' />
                </div>
    

                <button type="submit" className="btn btn-primary" style={{ marginLeft: 150 ,marginBlockEnd:20}} onClick={sign}>

                    {/* <Link to='/SidebarWhishlists'  >Sign In</Link> */}
                  Sign Up </button>
            </div>


        </>
    )
}