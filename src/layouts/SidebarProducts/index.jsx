// import React from 'react'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css'
import { GoPlus } from "react-icons/go";
import { BiTrash } from "react-icons/bi";



import { Modal } from 'antd';
import React, { useState ,useEffect} from 'react';
import 'antd/dist/antd.css';
import { requests } from '../../config/apirouter';
import { Axios } from '../../config/axios';

import './index.css'
import Navbar from '../Navbar';
export default () => {
 
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [Title, setTitle] = useState("")
  
  const [Product, setProduct] = useState([])

  const showModal = () => {
    setIsModalVisible(true);
  };


  useEffect(() => {
    let data = {
     Title: Title,

    }
    Axios.get(requests.ProductsApi.all, data)
        .then(res => {
            if (res.status === 200) {
              setProduct(res.data.data)
            }
        })
}, [])
  return (
    <>


      <Navbar />


      <div className="navside" style={{ width: 300}} >

        <div style={{ width: 300, height: 700 }}>
          <div className="addwhish" style={{  }} >
            <a class="add" style={{ color: 'rgba(35, 140, 209, 0.753)', fontSize: 20,paddingTop:7}} onClick={showModal} href="/products/ajouterproduit">
              <GoPlus  style={{paddingRight:5}}/>
              
                Add Product </a>
    
            <br />
            {
              Product.map(P => {
                return (
                  <>
                    <br />
                    <a href="#" class="w3-bar-item w3-button" style={{ fontSize: 17, marginLeft: 20, color: 'black ' }} >
                    <Link to={`/products/${P.Title}`} activeClassName="" style={{color:'black'}}><span>{P.Title}</span></Link></a>

                    
                  </>
                )
              })
            }
          </div>
        </div>


      </div>

  

    </>
  )
}