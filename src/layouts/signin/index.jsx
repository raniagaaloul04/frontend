import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import './index.css'
import { useState } from 'react';
import { BiLogIn } from 'react-icons/bi';
import { Link } from 'react-router-dom'
import { Axios } from '../../config/axios';
import { requests } from '../../config/apirouter';

export default () => {
const [Name,setName] = useState("")
const [Password,setPassword] = useState("")
const [IsError,setIsError] = useState("")
const login = () => {

    let data = {
        Name : Name,
        Password : Password
    }

    Axios.post(requests.authenticationApi.signin , data)
    .then(res =>{
        console.log(res);
        window.location.href= '/whishes'
    } )
    .catch(err => {
        console.log(err.response);
        setIsError(true)
    })


}
    return (
        <>


            <div className="sign" style={{maxWidth:400,marginLeft:500,marginTop:150}}>
                <h3 class="pb-3" style={{marginLeft:150,marginTop:20}}>Sign In</h3>


{
    IsError 
    && 
    
    <div class="alert alert-danger" role="alert">
    The name or  password you entered is not associated with an account!
  </div>     
}

                <div className="mb-3">
                    <label className="form-label"style={{color:'gray',marginLeft:20}}>Name</label>
                    <br />

                    <input style={{marginLeft:20,maxWidth:300}} type="text" id="exampleInputName"  class="form-control"  value={Name} onChange={e=>setName(e.target.value)}/>

                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputPassword1" className="form-label" style={{color:'gray',marginLeft:20}}>Password</label>
                    <br />

                    <input type="password"style={{maxWidth:300,marginLeft:20}} id="exampleInputPassword1"  class="form-control"  value={Password} onChange={e=>setPassword(e.target.value)}/>
                </div>
                <div style={{marginLeft:260}}><a ><Link to='/signup' >Sign Up!</Link></a></div>

                <button type="submit" className="btn btn-primary" style={{ marginLeft: 150 ,marginBlockEnd:20}} onClick={login}>

                    
                  Sign In  </button>
            </div>


        </>
    )
}