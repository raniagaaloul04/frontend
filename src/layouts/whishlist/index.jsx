import React, { useState ,useEffect} from 'react';
import { useParams } from 'react-router';
import { requests } from '../../config/apirouter';
import { Axios } from '../../config/axios';


import Navbar from '../Navbar';
import SidebarWhishlists from '../SidebarWhishlists';

export default () => {
    const [products, setproducts] = useState([])
    const [Title, setTitle] = useState("")
    const [Description, setDescription] = useState("")
    const [Status, setStatus] = useState("")
    const [Price, setPrice] = useState("")
    const [Images, setImages] = useState([])
  
    let {Name} = useParams()
    useEffect(() => {
        let data = {
      Image:Image,
         Title: Title,
         Description:Description,
         Status:Status,
         Price:Price
    
        }
        Axios.get(requests.ProductsApi.all, data)
            .then(res => {
                if (res.status === 200) {
                    setproducts(res.data.data)
                }
            })
    }, [])

    return (
        <>

<SidebarWhishlists />
<div className="table1">
        <div className="class">
          <div className="whishlist" style={{ fontSize: 28, marginLeft: 20 }}>{Name}</div>


        </div>
        <br />
        <div className="nav2">
          <div className="nav">
            <a class="navbar-brand" href="#" style={{ fontSize: 20 }}>
              <div className="ToBuy" style={{ marginRight: 10 }}> </div>
              To Buy</a>
            <a class="navbar-brand" href="#" style={{ fontSize: 20 }}>
              <div className="Bought" style={{ marginRight: 5 }}> </div>
              Bought</a>
          </div>

        </div>
        <div class="table" >
          <table class="table table-borderless">
            <thead>
              <tr>
                <th ><h6>Image</h6></th>
                <th >Title</th>
                <th >Description</th>
                <th >Status</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>

            {
              products.map(p=> {
                return (
                  <>
                  <tr>
                <th scope="row">{
                Images.map((name)=>{return(<><img class="" src={name}></img></>)})}</th>
                <td>{p.Title}</td>
                <td>{p.Description}</td>
                <td>{p.Status}</td>
                <td>{p.Price}</td>
              </tr>
                    
                  </>
                )
              })
            }


           
            </tbody>
          </table>
        </div>
      </div>


        </>
    )
}