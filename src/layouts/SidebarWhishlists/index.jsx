// import React from 'react'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css'
import { GoPlus } from "react-icons/go";
import { BiTrash } from "react-icons/bi";



import { Modal } from 'antd';
import React, { useState ,useEffect} from 'react';
import 'antd/dist/antd.css';
import { requests } from '../../config/apirouter';
import { Axios } from '../../config/axios';

import './index.css'
import Navbar from '../Navbar';
export default () => {
 
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [Name, setName] = useState("")
  const [wishlist, setwishlist] = useState([])
  const [Product, setProduct] = useState([])

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const add = () => {

    let data = {
      Name: Name,

    }
    Axios.post(requests.AddWhishlistApi.add, data)
      .then(res => {
        console.log(res);
        let arr = [...wishlist]
        arr.push(res.data.data)
        setwishlist(arr)

      })
      .catch(err => {
        console.log(err.response);

      })
  }
  
  useEffect(() => {
    let data = {
      Name: Name,

    }
    Axios.get(requests.AddWhishlistApi.all, data)
        .then(res => {
            if (res.status === 200) {
              setwishlist(res.data.data)
            }
        })
}, [])
  return (
    <>


      <Navbar />


      <div className="navside" style={{ width: 300}} >

        <div style={{ width: 300, height: 700 }}>
          <div className="addwhish" style={{  }} >
            <a class="add" style={{ color: 'rgba(35, 140, 209, 0.753)', fontSize: 20,paddingTop:7}} onClick={showModal}>
              <GoPlus  style={{paddingRight:5}}/>
              
              Add Whishlit  </a>
            <Modal footer={null} title="Add Whishlist" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
              <div className="mb-3">
                <label className="form-label" style={{ color: 'gray', marginLeft: 20 }} >Name</label>
                <br />

                <input style={{ marginLeft: 20, maxWidth: 300 }} type="text" id="exampleInputName" class="form-control" value={Name} onChange={e => setName(e.target.value)} />
                <br />
                <div class="modal-footer">
                <button type="submit" className="btn btn-outline-primary" onClick={handleCancel} style={{ marginLeft: 150,marginTop:8 }} >Cancel</button>
                <button type="submit" className="btn btn-primary"onClick={add}>
                  Done</button>
              </div>
              </div>
            </Modal>
            <br />
            {
              wishlist.map(w => {
                return (
                  <>
                    <br />
                    <a href="#" class="w3-bar-item w3-button" style={{ fontSize: 17, marginLeft: 20, color: 'black ' }} >
                    <Link to={`/whishes/${w.Name}`} activeClassName="" style={{color:'black'}}><span>{w.Name}</span></Link></a>
                    
                  </>
                )
              })
            }
          </div>
        </div>


      </div>

  

    </>
  )
}