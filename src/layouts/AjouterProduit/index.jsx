// import React from 'react'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css'
import { GoPlus } from "react-icons/go";
import { BiTrash } from "react-icons/bi";

import { Button, FormControl,Form,InputGroup,Row,Col, } from "react-bootstrap";
import './index.css'
import { Modal } from 'antd';
import React, { useState ,useEffect} from 'react';
import 'antd/dist/antd.css';
import { requests } from '../../config/apirouter';
import { Axios } from '../../config/axios';


import SidebarProducts from '../SidebarProducts';
export default () => {
    const [Name, setName] = useState("")
    const [wishlist, setwishlist] = useState([])
    const [Whishlist, setWhishlist] = useState([])

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [Title, setTitle] = useState("")
    const [Price, setPrice] = useState("")
    const [Description, setDescription] = useState("")
    const [Status, setStatus] = useState("")
    const [Currency, setCurrency] = useState("")
    const [Product, setProduct] = useState([])

    const nameList=wishlist.map(Name => {
      return (
        <>
       {Name}
          
        </>
      )
    })
    
    const showModal = () => {
      setIsModalVisible(true);
    };
    const handleCancel = () => {
      setIsModalVisible(false);
    };
    useEffect(() => {
        let data = {
          Name: Name,
    
        }
        Axios.get(requests.AddWhishlistApi.all, data)
            .then(res => {
                if (res.status === 200) {
                  setwishlist(res.data.data)
                }
            })
    }, [])
    const add = () => {

      let data = {
        Title:Title,
        Price:Price,
        Currency:Currency,
        Description:Description,
        Whishlist:Whishlist,
        Status:Status
  
      }
      Axios.post(requests.ProductsApi.add, data)
        .then(res => {
          console.log(res);
          let arr = [...Product]
          arr.push(res.data.data)
          setProduct(arr)
  
        })
        .catch(err => {
          console.log(err.response);
  
        })
    }

  return (
    <>
     {/* margin-left: 310px;
    margin-top: -700px; */}

<SidebarProducts/>

<div className="AjouterProduit" style={{marginLeft:410,marginTop:-600,marginRight:410}}>
<div className="class"style={{marginLeft:150,marginTop:20}}><h1>Add Product</h1></div>
<form className="row g-3">



  <div className="col-md-4">
    <label htmlFor="inputState" className="form-label">Name</label>
    <input type="text" className="form-control" id="inputName" value={Title} onChange={e => setTitle(e.target.value)} />
  </div>

  <div className="col-md-6">
    <label htmlFor="inputPassword4" className="form-label" value={Price} onChange={e => setPrice(e.target.value)}>Price</label>
    <input type="password" className="form-control" id="inputPassword4" />
  </div>
 
  <div className="col-md-2">
    <label htmlFor="inputZip" className="form-label" value={Currency} onChange={e => setCurrency(e.target.value)}>Currency</label>
    <select id="inputState" className="form-select">
      <option >TND</option>
      <option>USD</option>

      <option>EURO</option>
    </select>
  </div>
  <div className="col-12">
    <label htmlFor="inputAddress2" className="form-label">Description</label>
    <input type="text" className="form-control" id="inputAddress2" style={{height:120}}  value={Description} onChange={e => setDescription(e.target.value)}/>
  </div>

  <div className="col-md-6">
    <label htmlFor="inputZip" className="form-label" value={Whishlist} onChange={e => setWhishlist(e.target.value)}>Whishlist</label>
    <select id="inputState" className="form-select">
      <option >{nameList}</option>
     
    </select>
  </div>
  <div className="col-md-6" value={Status} onChange={e => setStatus(e.target.value)}>
    <label htmlFor="inputZip" className="form-label">Status</label>
    <select id="inputState" className="form-select">
      <option >To Buy</option>
      <option>Bougth</option>

    
    </select>
  </div>
  <div className="col-12">
  <button type="button" class="btn btn-outline-primary" onClick={handleCancel}>Cancel</button>

    <button type="submit" className="btn btn-primary"onClick={add}>Save</button>
  </div>
</form>

     
          </div>
          
  
         
    </>
  )
}